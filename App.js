import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './components/HomeScreen';
import ActivityIndicator from './components/ActivityIndicator';
import ButtonScreen from './components/Button';
import DetailsScreen from './components/Details';
import DrawerLayoutScreen from './components/DrawerLayoutAndroid';
import ImageScreen from './components/Image';
import KeyboardAvoidingViewScreen from './components/KeyboardAvoidingView';
import ListViewScreen from './components/ListView';
import ModalScreen from './components/Modal';
import PickerScreen from './components/Picker';
import ProgressBarScreen from './components/ProgressBarAndroid';
import RefreshControlScreen from './components/RefreshControl';
import ScrollViewScreen from './components/ScrollView';
import SectionListScreen from './components/SectionList';
import SliderScreen from './components/Slider';
import StatusBarScreen from './components/StatusBar';
import SwitchScreen from './components/Switch';
import TextScreen from './components/Text';
import TouchableHighlightScreen from './components/TouchableHighlight';
import TouchableNativeFeedbackScreen from './components/TouchNativeFeedback';
import ViewScreen from './components/View';
import WebViewScreen from './components/WebView';
//import screens here

const RootStack = createStackNavigator
(
  {
    Home:
    {
      screen: HomeScreen,
    },
    ActivityIndicator:
    {
      screen: ActivityIndicator,
    },
    Button:
    {
      screen: ButtonScreen,
    },
    Details:
    {
      screen: DetailsScreen,
    },
    DrawerLayout:
    {
      screen: DrawerLayoutScreen,
    },
    Image:
    {
      screen: ImageScreen,
    },
    KeyboardAvoidingView:
    {
      screen: KeyboardAvoidingViewScreen,
    },
    ListView:
    {
      screen: ListViewScreen,
    },
    Modal:
    {
      screen: ModalScreen,
    },
    Picker: 
    {
      screen: PickerScreen,
    },
    ProgressBar:
    {
      screen: ProgressBarScreen,
    },
    RefreshControl:
    {
      screen: RefreshControlScreen,
    },
    ScrollView:
    {
      screen: ScrollViewScreen,
    },
    SectionList:
    {
      screen: SectionListScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    StatusBar:
    {
      screen: StatusBarScreen,
    },
    Switch:
    {
      screen: SwitchScreen,
    },
    Text: {
      screen: TextScreen,
    },
    TouchableHighlight:
    {
      screen: TouchableHighlightScreen,
    },
    TouchableNativeFeedback:
    {
      screen: TouchableNativeFeedbackScreen,
    },
    View: {
      screen: ViewScreen,
    },
    WebView:
    {
      screen: WebViewScreen,
    }
  },
    {
      initialRoute: HomeScreen,
    },
)

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
