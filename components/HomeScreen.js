import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Button
              title="Go to Activity Indicator"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />

            <Button
              title="Go to Button"
              onPress={() => this.props.navigation.navigate('Button')}
            />

            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />

            <Button
              title="Go to Drawer Layout"
              onPress={() => this.props.navigation.navigate('DrawerLayout')}
            />

            <Button
              title="Go to Image"
              onPress={() => this.props.navigation.navigate('Image')}
            />

            <Button
              title="Go to Keyboard Avoiding View"
              onPress={() => this.props.navigation.navigate('KeyboardAvoidingView')}
            />

            <Button
              title="Go to List View"
              onPress={() => this.props.navigation.navigate('ListView')}
            />

            <Button
              title="Go to Modal"
              onPress={() => this.props.navigation.navigate('Modal')}
            />

            <Button
              title="Go to Picker"
              onPress={() => this.props.navigation.navigate('Picker')}
            />

            <Button
              title="Go to Progress Bar (Android)"
              onPress={() => this.props.navigation.navigate('ProgressBar')}
            />

            <Button
              title="Go to Refresh Control"
              onPress={() => this.props.navigation.navigate('RefreshControl')}
            />

            <Button
              title="Go to Scroll View"
              onPress={() => this.props.navigation.navigate('ScrollView')}
            />

            <Button
              title="Go to Section List"
              onPress={() => this.props.navigation.navigate('SectionList')}
            />

            <Button
              title="Go to Slider"
              onPress={() => this.props.navigation.navigate('Slider')}
            />

            <Button
              title="Go to Status Bar"
              onPress={() => this.props.navigation.navigate('StatusBar')}
            />

            <Button
              title="Go to Switch"
              onPress={() => this.props.navigation.navigate('Switch')}
            />

            <Button
              title="Go to Text"
              onPress={() => this.props.navigation.navigate('Text')}
            />

            <Button
              title="Go to Touchable Highlight"
              onPress={() => this.props.navigation.navigate('TouchableHighlight')}
            />

            <Button
              title="Go to Touch Native Feedback"
              onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
            />

            <Button
              title="Go to View"
              onPress={() => this.props.navigation.navigate('View')}
            />

            <Button
              title="Go to Web View"
              onPress={() => this.props.navigation.navigate('WebView')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
});
  
HomeScreen.propTypes = {

};

export default HomeScreen;