import React, { Component, PropTypes } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default class App extends Component {
    render() {
        return (
            <View style={[styles.container, styles.horizontal]}>
                <Button
                    onPress={() => this.props.navigation.navigate('Home')}
                    title="Go back"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center'
    }, /*
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    } */
})